# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'twisted-17.5.0-r1.ebuild' from Gentoo, which is:
#    Copyright 1999-2017 Gentoo Foundation

MY_PN=${PN,}
MY_PNV=${MY_PN}-${PV}

local binaries=( cftp ckeygen conch mailmail pyhtmlizer tkconch trial twist
                 twistd )

require pypi py-pep517 [ backend=hatchling entrypoints=[ "${binaries[@]}" ] work=${MY_PNV} ]
require flag-o-matic
require utf8-locale

SUMMARY="Twisted is an event-driven networking engine"
DESCRIPTION="
Twisted is an event-based framework for internet applications. It includes modules for many
different purposes, including the following: twisted.web: HTTP clients and servers, HTML
templating, and a WSGI server twisted.conch: SSHv2 and Telnet clients and servers and terminal
emulators twisted.words: Clients and servers for IRC, XMPP, and other IM protocols twisted.mail:
IMAPv4, POP3, SMTP clients and servers twisted.positioning: Tools for communicating with
NMEA-compatible GPS receivers twisted.names: DNS client and tools for making your own DNS servers
twisted.trial: A unit testing framework that integrates well with Twisted-based code.
"
HOMEPAGE+=" https://twistedmatrix.com"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

#TODO: soap, http2
DEPENDENCIES="
    build:
        dev-python/hatch_fancy_pypi_readme[>=22.5.0][python_abis:*(-)?]
    build+run:
        dev-python/Automat[>=0.8.0][python_abis:*(-)?]
        dev-python/attrs[>=21.3.0][python_abis:*(-)?]
        dev-python/constantly[>=15.1][python_abis:*(-)?]
        dev-python/hyperlink[>=17.1.1][python_abis:*(-)?]
        dev-python/idna[>=2.4][python_abis:*(-)?]
        dev-python/incremental[>=22.10.0][python_abis:*(-)?]
        dev-python/pyopenssl[>=21.0.0][python_abis:*(-)?]
        dev-python/service_identity[>=18.1.0][python_abis:*(-)?]
        dev-python/typing-extensions[>=4.2.0][python_abis:*(-)?]
        dev-python/zopeinterface[>=5][python_abis:*(-)?]
        (
            !net-twisted/TwistedConch
            !net-twisted/TwistedCore
            !net-twisted/TwistedLore
            !net-twisted/TwistedMail
            !net-twisted/TwistedNames
            !net-twisted/TwistedNews
            !net-twisted/TwistedRunner
            !net-twisted/TwistedWeb
            !net-twisted/TwistedWords
        ) [[
            *description = [ Old Twisted packages, now provided by Twisted ]
            *resolution = uninstall-blocked-before
        ]]
    test:
        dev-python/PyHamcrest[>=2][python_abis:*(-)?]
        dev-python/appdirs[>=1.4.0][python_abis:*(-)?]
        dev-python/bcrypt[>=3.1.3][python_abis:*(-)?]
        dev-python/cryptography[>=3.3][python_abis:*(-)?]
        dev-python/cython-test-exception-raiser[>=1.0.2&<2][python_abis:*(-)?]
        dev-python/hypothesis[>=6.56][python_abis:*(-)?]
        dev-python/pyserial[>=3.0][python_abis:*(-)?]
    suggestion:
        (
            dev-python/appdirs[>=1.4.0][python_abis:*(-)?]
            dev-python/bcrypt[>=3.1.3][python_abis:*(-)?]
            dev-python/cryptography[>=3.3][python_abis:*(-)?]
        ) [[
            *description = [ Required for Twisted's SSHv2 implementation ]
        ]]
        dev-python/pyserial[>=3.0][python_abis:*(-)?] [[
            description = [ Use pySerial to provide support for an asynchronous serial port transport ]
        ]]
"

# I tried to get the tests to run, but there many which want to bind to 0.0.0.0
RESTRICT="test"

pkg_setup() {
    # required for tests
    require_utf8_locale
}

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    edo sed \
        -e 's:test_allowedAnonymousClientConnection:_&:' \
        -e 's:test_ellipticCurveDiffieHellman:_&:' \
        -e 's:test_successful:_&:' \
        -e 's:test_verification:_&:' \
        -i src/twisted/test/test_sslverify.py
}

test_one_multibuild() {
    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" \
    edo ${PYTHON} -m twisted.trial \
        -j ${EXJOBS:-1} \
        --reporter=verbose twisted \
        --temp-directory=${TEMP}/_trial_temp
}

