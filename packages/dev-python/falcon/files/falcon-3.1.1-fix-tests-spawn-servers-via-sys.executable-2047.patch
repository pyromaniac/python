Upstream: yes

From d04c4c4a0e2b2e2a6aaef874141b64abc2994e75 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Micha=C5=82=20G=C3=B3rny?= <mgorny@gentoo.org>
Date: Wed, 30 Mar 2022 09:47:28 +0200
Subject: [PATCH] fix(tests): spawn servers via sys.executable (#2047)

* fix(tests): spawn servers via sys.executable

Spawn the servers (gunicorn, uvicorn, etc.) during tests through
sys.executable rather than directly, in order to ensure that the test
server is run using the same Python executable as the test suite.
Otherwise, the test servers are started using the Python executable
specified in their shebang and can therefore escape the virtualenv they
are being run in.

This is particularly problem for tests being run from Gentoo ebuilds.
We create a --system-site-packages venv to install falcon in, in order
to test whether it works correctly against the dependencies installed
on the system prior to installing it.  However, the installed packages
such as gunicorn specify /usr/bin/python* shebangs that override
the venv and therefore make these servers unable to find venv-installed
falcon.  Running them via sys.executable ensures that they are started
through Python installed in the venv.
---
 docs/_newsfragments/2047.bugfix.rst |  2 ++
 examples/look/tests/conftest.py     | 10 +++++++++-
 tests/asgi/test_asgi_servers.py     | 12 +++++++++++-
 tests/test_wsgi_servers.py          |  8 +++++++-
 4 files changed, 29 insertions(+), 3 deletions(-)
 create mode 100644 docs/_newsfragments/2047.bugfix.rst

diff --git a/docs/_newsfragments/2047.bugfix.rst b/docs/_newsfragments/2047.bugfix.rst
new file mode 100644
index 0000000..9e66717
--- /dev/null
+++ b/docs/_newsfragments/2047.bugfix.rst
@@ -0,0 +1,2 @@
+The web servers used for tests are now run through :any:`sys.executable` in
+order to ensure that they respect the virtualenv in which tests are being run.
diff --git a/examples/look/tests/conftest.py b/examples/look/tests/conftest.py
index ed25de1..e7ce3f2 100644
--- a/examples/look/tests/conftest.py
+++ b/examples/look/tests/conftest.py
@@ -1,5 +1,6 @@
 import os
 import subprocess
+import sys
 import time
 
 import requests
@@ -14,7 +15,14 @@ def pytest_sessionstart(session):
     global gunicorn
 
     gunicorn = subprocess.Popen(
-        ('gunicorn', '--pythonpath', LOOK_PATH, 'look.app:get_app()'),
+        (
+            sys.executable,
+            '-m',
+            'gunicorn',
+            '--pythonpath',
+            LOOK_PATH,
+            'look.app:get_app()',
+        ),
         env=dict(os.environ, LOOK_STORAGE_PATH='/tmp'),
     )
 
diff --git a/tests/asgi/test_asgi_servers.py b/tests/asgi/test_asgi_servers.py
index bb1d419..26f51ad 100644
--- a/tests/asgi/test_asgi_servers.py
+++ b/tests/asgi/test_asgi_servers.py
@@ -500,7 +500,13 @@ uvicorn.run('_asgi_test_app:application', host='{host}', port={port})
     )
 
     return subprocess.Popen(
-        ('uvicorn',) + loop_options + options,
+        (
+            sys.executable,
+            '-m',
+            'uvicorn',
+        )
+        + loop_options
+        + options,
         cwd=_MODULE_DIR,
     )
 
@@ -508,6 +514,8 @@ uvicorn.run('_asgi_test_app:application', host='{host}', port={port})
 def _daphne_factory(host, port):
     return subprocess.Popen(
         (
+            sys.executable,
+            '-m',
             'daphne',
             '--bind',
             host,
@@ -543,6 +551,8 @@ run(config)
         )
     return subprocess.Popen(
         (
+            sys.executable,
+            '-m',
             'hypercorn',
             '--bind',
             f'{host}:{port}',
diff --git a/tests/test_wsgi_servers.py b/tests/test_wsgi_servers.py
index 568eaaa..c6b489e 100644
--- a/tests/test_wsgi_servers.py
+++ b/tests/test_wsgi_servers.py
@@ -28,6 +28,8 @@ def _gunicorn_args(host, port, extra_opts=()):
         pytest.skip('gunicorn not installed')
 
     args = (
+        sys.executable,
+        '-m',
         'gunicorn',
         '--access-logfile',
         '-',
@@ -72,6 +74,8 @@ def _uvicorn_args(host, port):
         pytest.skip('uvicorn not installed')
 
     return (
+        sys.executable,
+        '-m',
         'uvicorn',
         '--host',
         host,
@@ -102,7 +106,9 @@ def _waitress_args(host, port):
         pytest.skip('waitress not installed')
 
     return (
-        'waitress-serve',
+        sys.executable,
+        '-m',
+        'waitress',
         '--listen',
         '{}:{}'.format(host, port),
         '_wsgi_test_app:app',
-- 
2.39.0

