# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=setuptools test=pytest ]

SUMMARY="py.test plugin to test server connections locally"
HOMEPAGE+=" https://github.com/pytest-dev/pytest-localserver"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[>=3.4.1][python_abis:*(-)?]
    build+run:
        dev-python/Werkzeug[>=0.10][python_abis:*(-)?]
    test:
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        python_abis:3.8? ( dev-python/pytest[>=4&<8][python_abis:3.8] )
        python_abis:3.9? ( dev-python/pytest[>=4&<8][python_abis:3.9] )
        python_abis:3.10? ( dev-python/pytest[>=6.2.4&<8][python_abis:3.10] )
"

PYTEST_PARAMS=(
    # Avoid aiosmtpd, see below
    --ignore tests/test_smtp.py
)

prepare_one_multibuild() {
    edo sed -e '/"aiosmtpd",/d' -i setup.py

    py-pep517_prepare_one_multibuild
}

test_one_multibuild() {
    esandbox allow_net "inet:127.0.0.1@30000-49999"
    esandbox allow_net --connect "inet:127.0.0.1@30000-49999"

    py-pep517_test_one_multibuild

    esandbox disallow_net --connect "inet:127.0.0.1@30000-49999"
    esandbox disallow_net "inet:127.0.0.1@30000-49999"
}

