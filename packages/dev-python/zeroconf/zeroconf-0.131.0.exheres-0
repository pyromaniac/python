# Copyright 2016-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="python-${PN}"

require github [ user=jstasiak ]
require py-pep517 [ backend=poetry-core backend_version_spec='[>=1.5.2]' test=pytest work=${MY_PN}-${PV} ]

SUMMARY="Pure Python Multicast DNS Service Discovery Library (Bonjour/Avahi compatible)"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/Cython[>=3.0.5][python_abis:*(-)?]
        dev-python/setuptools[>=65.4.1][python_abis:*(-)?]
        dev-python/wheel[python_abis:*(-)?]
    build+run:
        dev-python/async-timeout[>=4.0.1][python_abis:*(-)?]
        dev-python/ifaddr[>=0.1.7][python_abis:*(-)?]
"

# fail on CI, last checked: 0.28.6
PYTEST_SKIP=(
    test_launch_and_close
    test_launch_and_close_v4_v6
    test_launch_and_close_v6_only
    test_integration_with_listener_ipv6
)

test_one_multibuild() {
    esandbox allow_net --bind "inet:0.0.0.0@5353"
    esandbox allow_net --connect "inet:224.0.0.251@5353"

    setup-py_test_one_multibuild

    esandbox disallow_net --connect "inet:224.0.0.251@5353"
    esandbox disallow_net --bind "inet:0.0.0.0@5353"
}

