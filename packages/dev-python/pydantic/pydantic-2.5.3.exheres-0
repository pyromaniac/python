# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=hatchling test=pytest ]

SUMMARY="Data parsing and validation using Python type hints"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: optional dependency on unpackaged email-validator
DEPENDENCIES="
    build:
        dev-python/hatch_fancy_pypi_readme[>=22.5.0][python_abis:*(-)?]
    build+run:
        dev-python/annotated-types[>=0.4.0][python_abis:*(-)?]
        dev-python/pydantic-core[>=2.14.6][python_abis:*(-)?]
        dev-python/typing-extensions[>=4.6.1][python_abis:*(-)?]
    test:
        dev-python/coverage[python_abis:*(-)?]
        dev-python/dirty-equals[python_abis:*(-)?]
        dev-python/Faker[>=18.13.0][python_abis:*(-)?]
        dev-python/pytest-benchmark[>=4.0.0][python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # Avoid a dep on the unwritten pytest-examples
    --ignore tests/test_docs.py
    # Fails, "updating Faker will almost certainly change the benchmark data"
    --ignore tests/benchmarks/test_north_star.py
    # Needs the unpackages cloudpickle
    --ignore tests/test_pickle.py
)
PYTEST_SKIP=(
    # Needs the unpackaged email-validator
    test_fastapi_startup_perf
)

