# Copyright 2009, 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=setuptools test=pytest \
    python_opts="[readline][sqlite]" entrypoints=[ ${PN}{,3} ] ]

export_exlib_phases src_install

SUMMARY="Interactive computing environment for Python"
DESCRIPTION="
IPython provides a rich architecture for interactive computing with:

- A powerful interactive shell.
- A kernel for Jupyter.
- Support for interactive data visualization and use of GUI toolkits.
- Flexible, embeddable interpreters to load into your own projects.
- Easy to use, high performance tools for parallel computing.
"
HOMEPAGE+=" https://ipython.org/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    matplotlib [[ description = [ Install matplotlib to provide plotting capabilities ] ]]
    nbconvert [[ description = [ Tool to convert notebooks to other formats ] ]]
    notebook [[ description = [ Install the web-based notebook backend ] ]]
    parallel [[ description = [ Enable parallel distributed computing features ] ]]
    qtconsole [[ description = [ Qt-based console with rich media output ] ]]
"

DEPENDENCIES="
    build+run:
        dev-python/backcall[python_abis:*(-)?]
        dev-python/decorator[python_abis:*(-)?]
        dev-python/matplotlib-inline[python_abis:*(-)?]
        dev-python/pexpect[>4.3][python_abis:*(-)?]
        dev-python/pickleshare[python_abis:*(-)?]
        dev-python/prompt_toolkit[>=3.0.30&<3.1.0][python_abis:*(-)?]
        dev-python/Pygments[>=2.4.0][python_abis:*(-)?]
        dev-python/stack-data[python_abis:*(-)?]
        dev-python/simplegeneric[>0.8][python_abis:*(-)?]
        dev-python/traitlets[>=5][python_abis:*(-)?]
        dev-util/jedi[>=0.16][python_abis:*(-)?]
        matplotlib? (
            dev-python/matplotlib[python_abis:*(-)?]
        )
    post:
        nbconvert? (
            dev-python/nbconvert[python_abis:*(-)?]
        )
        notebook? (
            dev-python/ipywidgets[python_abis:*(-)?]
            dev-python/notebook[python_abis:*(-)?]
        )
        parallel? (
            dev-python/ipykernel[python_abis:*(-)?]
            dev-python/ipyparallel[python_abis:*(-)?]
        )
        qtconsole? (
            dev-python/ipykernel[python_abis:*(-)?]
            dev-python/qtconsole[python_abis:*(-)?]
        )
    test:
        dev-python/nbformat[python_abis:*(-)?]
        dev-python/numpy[>=1.21][python_abis:*(-)?]
        dev-python/pytest-asyncio[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/testpath[python_abis:*(-)?]
    suggestion:
        dev-python/ipykernel[python_abis:*(-)?] [[ description = [ test requirement ] ]]
"

test_one_multibuild() {
    if ! has_version "dev-python/ipykernel[python_abis:*(-)?]"; then
        ewarn "Test dependencies are not installed yet, skip tests"
        return
    fi

    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTHON} -m pytest
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    # Remove man page in wrong location and install it into the correct one
    # later.
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/share
}

ipython_src_install() {
    py-pep517_src_install

    doman "${WORK}"/PYTHON_ABIS/$(python_get_abi)/"${PYTHON_WORK}"/docs/man/*.1
}
