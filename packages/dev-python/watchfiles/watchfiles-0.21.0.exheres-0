# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo
# Tarball from pypi is missing docs/cli_help.txt, which is at least needed for
# the tests
require github [ user=samuelcolvin tag=v${PV} ]
require py-pep517 [ backend=maturin backend_version_spec="[>=0.14.16&<2]" test=pytest entrypoints=[ ${PN} ] ]

SUMMARY="Simple, modern and fast file watching and code reload in python"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/anyio[>=3.0.0][python_abis:*(-)?]
    test:
        dev-python/dirty-equals[python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
        dev-python/pytest-timeout[python_abis:*(-)?]
"

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    ecargo_fetch
}

test_one_multibuild() {
    edo rm -rf watchfiles

    py-pep517_test_one_multibuild
}

