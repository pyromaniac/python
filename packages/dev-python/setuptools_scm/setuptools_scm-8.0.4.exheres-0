# Copyright 2016-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/_/-}
MY_PNV=${MY_PN}-${PV}

require pypi
require py-pep517 [ backend=self-hosted blacklist=2 test=pytest work=${MY_PNV} ]

SUMMARY="Manage your versions by scm tags"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run+test:
        dev-python/packaging[>=20.0][python_abis:*(-)?]
        dev-python/typing-extensions[python_abis:*(-)?]
        python_abis:3.8? (
            dev-python/tomli[>=1.0.0][python_abis:3.8]
        )
        python_abis:3.9? (
            dev-python/tomli[>=1.0.0][python_abis:3.9]
        )
        python_abis:3.10? (
            dev-python/tomli[>=1.0.0][python_abis:3.10]
        )
    test:
        dev-python/build[python_abis:*(-)?]
        dev-python/wheel[python_abis:*(-)?]
        dev-scm/git
        dev-scm/mercurial
    suggestion:
        dev-python/rich[python_abis:*(-)?] [[
            description = [ Use rich as console log hander ]
        ]]
"

PYTEST_SKIP=(
    # Would need flake8
    test_dump_version_flake8
    # Wants to sign with commit and fails because there's no git repo (?)
    test_git_getdate_signed_commit
    # Test something by installing lz4 with pip, and thus wants to download
    # from the internet
    test_pip_download
    # Fails if nose in installed
    # https://github.com/pypa/setuptools_scm/issues/937
    test_pyproject_support
)

