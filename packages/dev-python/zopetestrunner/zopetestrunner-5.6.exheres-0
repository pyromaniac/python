# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=zope.testrunner
MY_PNV=${MY_PN}-${PV}

require pypi
require setup-py [ import=setuptools blacklist=2 work=${MY_PNV} ]

SUMMARY="A flexible test runner with layer support"

LICENCES="ZPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/six[python_abis:*(-)?]
        dev-python/zopeexceptions[python_abis:*(-)?]
        dev-python/zopeinterface[python_abis:*(-)?]
    post:
        dev-python/zopetesting[python_abis:*(-)?]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Causes an error during src_install
    edo rm src/zope/testrunner/tests/testrunner-ex/sample2/badsyntax.py

}

test_one_multibuild() {
    if has_version dev-python/zopetesting[python_abis:$(python_get_abi)]; then
        local abi_temp="${TEMP}${MULTIBUILD_TARGET}"
        local test_dir="${abi_temp}"/test
        local test_libdir="${test_dir}"/lib
        local test_log="${abi_temp}"/test.log

        # Install it to a temporary directory for testing, otherwise it picks
        # up the system installed variant
        edo mkdir -p "${test_libdir}"
        export PYTHONPATH="${test_libdir}"
        edo "${PYTHON}" setup.py install        \
            --home="${test_dir}"                \
        --install-lib="${test_libdir}"      \
        --single-version-externally-managed \
        --record="${abi_temp}"/record.log   \
        --no-compile

        edo pushd "${test_libdir}"

        PYTHONPATH="${test_libdir}" edo ${PYTHON} -m unittest discover -s zope/testrunner -t .

        edo popd
    else
        ewarn "Test dependency are not installed yet - skipping tests"
    fi
}

