# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=setuptools ]

SUMMARY="Pluggable search for Django"
DESCRIPTION="It provides modular search for Django. It features a unified,
familiar API that allows you to plug in different search backends (such as
Solr, Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.
Haystack is BSD licensed, plays nicely with third-party app without needing to
modify the source and supports advanced features like faceting, More Like This,
highlighting, spatial search and spelling suggestions."

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm[>=3.4][python_abis:*(-)?]
    build+run:
        dev-python/Django[>=2.2][python_abis:*(-)?]
    test:
        dev-python/coverage[python_abis:*(-)?]
        dev-python/geopy[>=2.0.0][python_abis:*(-)?]
        dev-python/pysolr[>=3.7.0][python_abis:*(-)?]
        dev-python/python-dateutil[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/Whoosh[>=2.5.4&<3.0][python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Use-standard-Django-test-runner.patch
)

prepare_one_multibuild() {
    py-pep517_test_one_multibuild

    # Allow newer geopy versions
    edo sed -e '/geopy/s/==2.0.0/>=2.0.0/' -i setup.py
}

test_one_multibuild() {
    PYTHONPATH=$(ls -d ${PWD}/build/lib*) edo ${PYTHON} -B setup.py test
}

