# Copyright 2023 Quentin “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=self-hosted test=pytest ]
SUMMARY="The awesome document factory"
DESCRIPTION="WeasyPrint is a smart solution helping web developers to create PDF documents.
It turns simple HTML pages into gorgeous statistical reports, invoices, tickets…

From a technical point of view, WeasyPrint is a visual rendering engine for HTML and CSS
that can export to PDF. It aims to support web standards for printing. WeasyPrint is free
software made available under a BSD license.

It is based on various libraries but not on a full rendering engine like WebKit or Gecko.
The CSS layout engine is written in Python, designed for pagination,
and meant to be easy to hack on."

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/cffi[>=0.6][python_abis:*(-)?]
        dev-python/cssselect2[>=0.1][python_abis:*(-)?]
        dev-python/fonttools[>=4.0.0][python_abis:*(-)?]
        dev-python/html5lib[>=1.1][python_abis:*(-)?]
        dev-python/Pillow[>=9.1.0][python_abis:*(-)?]
        dev-python/pydyf[>=0.6.0][python_abis:*(-)?]
        dev-python/pyphen[>=0.9.1][python_abis:*(-)?]
        dev-python/tinycss2[>=1.0.0][python_abis:*(-)?]
    test:
        dev-python/flake8[python_abis:*(-)?]
        dev-python/isort[python_abis:*(-)?]
"

