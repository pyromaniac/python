# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN}-py

require github [ user=elastic tag=v${PV} ]
require setup-py [ import=setuptools blacklist='2' test=pytest work=${MY_PN}-${PV} ]

SUMMARY="Python client for Elasticsearch"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/aiohttp[>=3&<4][python_abis:*(-)?]
        dev-python/elastic-transport[>=8&<9][python_abis:*(-)?]
        dev-python/requests[>=2.4.0&<3.0.0][python_abis:*(-)?]
    test:
        dev-python/mock[python_abis:*(-)?]
        dev-python/pytest-asyncio[python_abis:*(-)?]
        dev-python/python-dateutil[python_abis:*(-)?]
        dev-python/PyYAML[>=5.4][python_abis:*(-)?]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    edo sed \
        -e 's/--cov-report=term-missing --cov=elasticsearch --cov-config=.coveragerc//' \
        -i setup.cfg
}

