# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=2 test=pytest ]

SUMMARY="Pytest plugin for trio"
DESCRIPTION="
This is a pytest plugin to help you test projects that use Trio, a friendly
library for concurrency and async I/O in Python."

LICENCES="
    || ( Apache-2.0 MIT )
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/outcome[>=1.1.0][python_abis:*(-)?]
        dev-python/pytest[>=7.2.0][python_abis:*(-)?]
        dev-python/trio[>=0.22.0][python_abis:*(-)?]
"

test_one_multibuild() {
    # Two tests want to bind to 0.0.0.0@0
    PYTHONPATH=. edo py.test-$(python_get_abi) --pyargs pytest_trio \
        -p no:asyncio \
        -k "not test_async_yield_fixture_with_nursery and \
            not test_try"
}

