# Copyright 2013 Jorge Aparicio
# Copyright 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=PyCQA ]
require setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="The modular source code checker: pycodestyle, pyflakes and McCabe"
HOMEPAGE+=" https://gitlab.com/pycqa/${PN}"

UPSTREAM_DOCUMENTATION="https://${PN}.pycqa.org"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/mccabe[>=0.7.0&<0.8.0][python_abis:*(-)?]
        dev-python/pycodestyle[>=2.11.0&<2.12.0][python_abis:*(-)?]
        dev-python/pyflakes[>=3.1.0&<3.2.0][python_abis:*(-)?]
"

test_one_multibuild() {
    local abi_temp="${TEMP}${MULTIBUILD_TARGET}"
    local test_dir="${abi_temp}"/test
    local test_libdir="${test_dir}"/lib
    local test_log="${abi_temp}"/test.log

    # Install to register entry-points for built-in plugins
    edo mkdir -p "${test_libdir}"
    export PYTHONPATH="${test_libdir}"
    export PATH="${test_dir}/bin:${PATH}"
    edo ${PYTHON} -B setup.py install \
        --home="${test_dir}" \
        --install-lib="${test_libdir}"      \
        --single-version-externally-managed \
        --record="${abi_temp}"/record.log   \
        --no-compile
    # Some tests fail if flake8-import-order is installed due to deprecated
    # pkg_resources
    edo ${PYTHON} -B -m pytest \
        -k "not test_local_plugin_can_add_option \
            and not test_legacy_api" \
        --ignore tests/integration/test_main.py \
        --ignore tests/integration/test_plugins.py \
        tests

    unset PYTHONPATH
}
