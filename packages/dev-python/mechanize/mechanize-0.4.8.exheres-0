# Copyright 2008-2020 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import='setuptools' blacklist=2 ]

SUMMARY="Stateful programmatic web browsing in Python"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-python/html5lib[>=0.999999999][python_abis:*(-)?]
    test:
        dev-python/service_identity[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        net-twisted/Twisted[python_abis:*(-)?]
    suggestion:
        dev-python/html5-parser[>=0.4.4][python_abis:*(-)?] [[
            description = [ Faster HTML parsing ]
        ]]
"

test_one_multibuild() {
    edo unset http_proxy https_proxy ftp_proxy
    PYTHONPATH=$(ls -d build/lib*) edo "${PYTHON}" run_tests.py
}

# tests try to bind on 0.0.0.0:8000 and then 0.0.0.0:0
# even if we allowed the first, the latter chooses a random port which would
# need an `allow_net --connect` and we can't know what port to use
RESTRICT="test"

test_one_multibuild() {
    esandbox allow_net --connect "inet:127.0.0.1@8000"

    PYTHONPATH=$(ls -d build/lib*) edo "${PYTHON}" run_tests.py -v

    esandbox disallow_net --connect "inet:127.0.0.1@8000"
}

