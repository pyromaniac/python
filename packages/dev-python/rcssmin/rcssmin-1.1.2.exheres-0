# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=2 test=pytest ]

SUMMARY="A CSS minifier for Python"
DESCRIPTION="
The minifier is based on the semantics of the YUI compressor, which itself is
based on the rule list by Isaac Schlueter.
This module is a re-implementation aiming for speed instead of maximum
compression, so it can be used at runtime (rather than during a preprocessing
step). RCSSmin does syntactical compression only (removing spaces, comments
and possibly semicolons). It does not provide semantic compression (like
removing empty blocks, collapsing redundant properties etc). It does, however,
support various CSS hacks (by keeping them working as intended)."

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
"

