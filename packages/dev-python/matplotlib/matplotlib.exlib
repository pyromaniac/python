# Copyright 2011 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi

SETUP_CFG_COMMANDS="libs packages rc_options"
SETUP_CFG_libs_PARAMS=(
    "enable_lto = False"
    "system_freetype = True"
    "system_qhull = True"
)
SETUP_CFG_rc_options_PARAMS=(
    "backend = Agg"
)

require setup-py [ import=setuptools cfg_file=mplsetup python_opts='[tk?]' blacklist='2 3.7' ]

SUMMARY="Comprehensive library for creating static, animated, and interactive visualizations"
DESCRIPTION="
Matplotlib produces publication quality figures in a variety of hardcopy formats and interactive
environments across platforms. matplotlib can be used in python scripts, the python and ipython
shell (ala MATLAB or Mathematica), web application servers, and six graphical user interface
toolkits.

matplotlib tries to make easy things easy and hard things possible. You can generate plots,
histograms, power spectra, bar charts, errorcharts, scatterplots, etc, with just a few lines of
code.
"
HOMEPAGE+=" https://matplotlib.org"

LICENCES="PSF"
SLOT="0"
MYOPTIONS="
    cairo [[ description = [ Add support utilizing Cairo ] ]]
    gtk3 [[ description = [ Add GUI support utilizing GTK3 ] ]]
    latex [[ description = [ Add support for LaTeX ] ]]
    qt5 [[ description = [ Add GUI support utilizing Qt5 ] ]]
    qt6 [[ description = [ Add GUI support utilizing Qt6 ] ]]
    tk [[ description = [ Add GUI support utilizing Tk ] ]]
    webagg [[ description = [ Add support for the web backend ] ]]
    wxwidgets [[ description = [ Add GUI support utilizing wxWidgets ] ]]
"

# Tests are tightly bound to the specific version of libfreetype
# and aggressively fail when rendered with other versions text differs from expected.
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[>=7][python_abis:*(-)?]
        virtual/pkg-config
    build+run:
        dev-libs/pybind11[>=2.6.0][python_abis:*(-)?]
        dev-python/contourpy[>=1.0.1][python_abis:*(-)?]
        dev-python/cycler[>=0.10][python_abis:*(-)?]
        dev-python/fonttools[>=4.22.0][python_abis:*(-)?]
        dev-python/kiwisolver[>=1.0.1][python_abis:*(-)?]
        dev-python/numpy[>=1.20][python_abis:*(-)?]
        dev-python/packaging[>=20.0][python_abis:*(-)?]
        dev-python/Pillow[>=6.2.0][python_abis:*(-)?]
        dev-python/pyparsing[>2.3.1][python_abis:*(-)?]
        dev-python/python-dateutil[>=2.7][python_abis:*(-)?]
        media-libs/freetype:2[>=2.4]
        media-libs/libpng:=[>=1.2]
        sci-libs/qhull[>=2020.2]
        python_abis:3.8? ( dev-python/importlib_resources:0[>=3.2.0][python_abis:3.8] )
        python_abis:3.9? ( dev-python/importlib_resources:0[>=3.2.0][python_abis:3.9] )
    run:
        cairo? ( dev-python/pycairo[>=1.14.0][python_abis:*(-)?] )
        gtk3? (
            gnome-bindings/pygobject:3[cairo][python_abis:*(-)?]
            x11-libs/gtk+:3[gobject-introspection]
        )
        latex? (
            app-text/dvipng
            app-text/ghostscript
            app-text/poppler
            dev-texlive/texlive-latex
        )
        qt5? ( dev-python/PyQt5[python_abis:*(-)?] )
        qt6? ( dev-python/PyQt6[>=6.1][python_abis:*(-)?] )
        tk? ( dev-lang/tk[>=8.4] )
        webagg? ( dev-python/tornado[>=5][python_abis:*(-)?] )
        wxwidgets? ( dev-python/wxPython:*[>=4][python_abis:*(-)?] )
"

configure_one_multibuild() {
    if expecting_tests; then
        SETUP_CFG_packages_PARAMS=(
            "tests = True"
        )
    fi

    setup-py_configure_one_multibuild
}

