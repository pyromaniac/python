# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=python-trio ]
require setup-py [ import=setuptools blacklist=2 test=pytest ]

SUMMARY="WebSocket client and server implementation for Python Trio"
DESCRIPTION="
This library implements both server and client aspects of the the WebSocket
protocol, striving for safety, correctness, and ergonomics. It is based on the
wsproto project, which is a Sans-IO state machine that implements the majority
of the WebSocket protocol, including framing, codecs, and events. This library
handles I/O using the Trio framework. This library passes the Autobahn Test
Suite."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/exceptiongroup[python_abis:*(-)?]
        dev-python/trio[>=0.11][python_abis:*(-)?]
        dev-python/wsproto[>=0.14][python_abis:*(-)?]
    test:
        dev-python/pytest-trio[>=0.5.0][python_abis:*(-)?]
        dev-python/trustme[python_abis:*(-)?]
"

PYTEST_PARAMS=( -p trio )

test_one_multibuild() {
    export PYTEST_DISABLE_PLUGIN_AUTOLOAD=1

    setup-py_test_one_multibuild
}

