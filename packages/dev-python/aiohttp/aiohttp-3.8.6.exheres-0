# Copyright 2016-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=setuptools test=pytest ]

SUMMARY="HTTP client/server for asyncio"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/aiosignal[>=1.1.2][python_abis:*(-)?]
        dev-python/async-timeout[>=4.0&<5.0][python_abis:*(-)?]
        dev-python/attrs[>=17.3.0][python_abis:*(-)?]
        dev-python/charset-normalizer[>=2.0&<4.0][python_abis:*(-)?]
        dev-python/frozenlist[>=1.1.1][python_abis:*(-)?]
        dev-python/multidict[>=4.5&<7.0][python_abis:*(-)?]
        dev-python/yarl[>=1.0&<2.0][python_abis:*(-)?]
    test:
        app-arch/brotli[python][python_abis:*(-)?]
        dev-python/freezegun[python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
        dev-python/pytest-xdist[python_abis:*(-)?]
        dev-python/re-assert[python_abis:*(-)?]
        dev-python/trustme[python_abis:*(-)?]
        www-servers/gunicorn[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # We don't have proxy-py, https://pypi.org/project/proxy.py/
    --ignore=tests/test_proxy_functional.py
    # Causes a sydbox violation during collection
    --ignore=tests/test_run_app.py
)

PYTEST_SKIP=(
    # Needs internet/network
    test_addresses[pyloop]
    test_client_session_timeout_zero
    test_ctor_global_loop
    test_default_loop[pyloop]
    test_set_loop_default_loop
    test_unix_connector[pyloop]
    test_unsupported_upgrade[pyloop]
    # asserts
    test_c_parser_loaded
    # fail on CI
    test_yield_from_in_session_request[pyloop]
    test_aiohttp_request_ctx_manager_not_found
)

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    edo sed -e '/--cov=/d' -i setup.cfg
}

