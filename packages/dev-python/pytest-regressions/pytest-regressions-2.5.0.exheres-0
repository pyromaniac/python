# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="Pytest plugin for regression testing"
DESCRIPTION="
This plugin makes it simple to test general data, images, files, and numeric tables by saving expected data in a data directory (courtesy of pytest-datadir) that can be used to verify that future runs produce the same data.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/pytest[>=6.2.0][python_abis:*(-)?]
        dev-python/pytest-datadir[python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]

"

# Complain about test files not being YAML list
RESTRICT="test"

PYTEST_PARAMS=(
    # Would need pandas
    --ignore tests/test_dataframe_regression.py
    --ignore tests/test_num_regression.py
)

PYTEST_SKIP=(
    # Would need pandas
    tests/test_filenames.py::test_foo
    tests/test_filenames.py::TestClass::test_foo
    tests/test_filenames.py::TestClassWithIgnoredName::test_foo
)

