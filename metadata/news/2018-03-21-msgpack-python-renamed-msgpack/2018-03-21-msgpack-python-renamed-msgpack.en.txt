Title: dev-python/msgpack-python has been renamed to dev-python/msgpack
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2018-03-21
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-python/msgpack-python

Please install dev-python/msgpack and *afterwards* uninstall dev-python/msgpack-python.

1. Take note of any packages depending on dev-python/msgpack-python:
cave resolve \!dev-python/msgpack-python

2. Install dev-python/msgpack:
cave resolve dev-python/msgpack -x1

3. Re-install the packages from step 1.

4. Uninstall dev-python/msgpack-python
cave resolve \!dev-python/msgpack-python -x

Do it in *this* order or you'll potentially *break* your system.
